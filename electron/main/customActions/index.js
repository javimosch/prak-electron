import getAppVersion from './getAppVersion';
import testDirectus from './testDirectus';
import saveSourceItems from './saveSourceItems';
import saveConfigurationProperty from './saveConfigurationProperty';
import setConfigValues from './setConfigValues'
export default {
    getAppVersion,
    testDirectus,
    saveSourceItems,
    saveConfigurationProperty,
    setConfigValues
}